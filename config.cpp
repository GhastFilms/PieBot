//here be dragons
#include "config.h"
#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include "api_machine.h"
#include "json.hpp"
#include "classes/redditPost.h"

//c++ is being annoying so i did this :/
std::vector<std::string> split2(std::string str2,std::string sep2){
    char* cstr2=const_cast<char*>(str2.c_str());
    char* current1;
    std::vector<std::string> arr2;
    current1=std::strtok(cstr2,sep2.c_str());
    while(current1!=NULL){
        arr2.push_back(current1);
        current1=std::strtok(NULL,sep2.c_str());
    }
    return arr2;
}

void config::init() {
	std::string line;
	bool colectingInsults = false;
	bool colectingModerators = false;
	std::ifstream myfile ("config.txt");
	if (myfile.is_open())
  	{
    	while ( getline (myfile,line) )
    	{
			if(line.find("#end") != std::string::npos) {
				colectingInsults = false;
				colectingModerators = false;
			}
			else if(line.find("#insults") != std::string::npos) {
				colectingInsults = true;		
			}
			else if(line.find("#moderators") != std::string::npos) {
				colectingModerators = true;
			}
			else if(colectingInsults) {
				insults.push_back(line);
			}
			else if(colectingModerators) {
				moderators.push_back(line);
				std::cout << "added moderator: " << line << std::endl;
			}
      		else if(line.find("prefix:") != std::string::npos) {
				prefix = line.erase(0,7);
			}
			else if(line.find("modPrefix:") != std::string::npos) {
				modPrefix = line.erase(0,10);
			}
			else if(line.find("token:") != std::string::npos) {
				token = line.erase(0,6);
				if(token == "token") {
					std::cout << "enter bot token in the config file" << std::endl;
				}
			}
			//groups
			else if(line.find("ownerID:") != std::string::npos) {
				ownerID = line.erase(0,8); 
			}
			//subreddit commands
			else if(line.find("memeSubreddit:") != std::string::npos) {
				line.erase(0,14);
				std::vector<std::string> arr = split2(line, ",");
				//std::cout << arr[1] << std::endl;
				for(auto i : arr) {
					std::cout << "Collecting info from: " << i << std::endl;
					std::vector<redditPost> temparr = getRPostObjs("https://www.reddit.com/" + i + "/hot.json?limit=100");
					std::cout << "Collected top " << temparr.size() << " posts" << std::endl;
					memes.insert(memes.end(), temparr.begin(), temparr.end());
				}
			}
			else if(line.find("catsSubreddit:") != std::string::npos) {
				line.erase(0,14);
				std::vector<std::string> arr = split2(line, ",");	
					for(auto i : arr) {
						std::cout << "Collecting info from: " << i << std::endl;
						std::vector<redditPost> temparr = getRPostObjs("https://www.reddit.com/" + i + "/hot.json?limit=100");
						std::cout << "Collected top " << temparr.size() << " posts" << std::endl;
						cats.insert(cats.end(), temparr.begin(), temparr.end());
				}
			}
    	}
		myfile.close();
  	}
	moderators.push_back(ownerID);
}

bool config::isModerator(std::string ID) {
	for(std::string i : moderators) {
		if(i == ID) {
			return true;
		
		}
	}
	return false;
}

std::vector<redditPost> config::getMemes() {
	return memes;
}

std::vector<redditPost> config::getCats() {
	return cats;
}

std::string config::getOwnerID() {
	return ownerID;
}

std::string config::getToken() {
	return token;
}

std::string config::getPrefix() {
	return prefix;
}
std::string config::getModPrefix() {
	return modPrefix;
}

std::vector<std::string> config::getInsults() {
	return insults;
}
std::vector<std::string> config::getModerators() {
	return moderators;
}

//int main() {config cfg; cfg.init(); std::cout << cfg.getOwnerID() << std::endl;return 0;}
