#ifndef API_MACHINE
#define API_MACHINE

#include <string>
#include <vector>
#include "classes/redditPost.h"
#include "json.hpp"

std::vector<redditPost> getRPostObjs(std::string);
std::string httpGet(std::string);
std::string rq(std::string);

#endif