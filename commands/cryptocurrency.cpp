#include "cryptocurrency.h"
#include "sleepy_discord/websocketpp_websocket.h"
#include "../json.hpp"
#include <vector>
#include "../api_machine.h"
#include "../config.h"

std::string cryptoCompare(SleepyDiscord::Message msg, config conf, std::vector<std::string> splitmsg) {
    if(splitmsg[1] == "price") {
		std::string coin = "BTC";
    	if(splitmsg.size() >= 3) {
		coin = splitmsg[2];
		transform(coin.begin(), coin.end(), coin.begin(), toupper);
		nlohmann::json j = nlohmann::json::parse(httpGet("https://min-api.cryptocompare.com/data/price?fsym=" + coin + "&tsyms=USD"));
		if(j["USD"].dump() != "null") {
		    return msg.channelID, "Current " + coin + " price: $`" + j["USD"].dump() + "`";
		}
		else {
				return msg.channelID, "Invalid Crypto Currency";
			}
		}
	}
	else{
		return msg.channelID, "Invalid use of `"+ conf.getPrefix() + "cc price` \\nCorrect use: `" + conf.getPrefix() + "cc price btc`";
	}	
}