#include "anime.h"
#include "sleepy_discord/websocketpp_websocket.h"
#include "../json.hpp"
#include <vector>
#include "../api_machine.h"
#include "../config.h"

std::string myAnimeList(SleepyDiscord::Message msg, config conf, std::vector<std::string> splitmsg) {   
	if(splitmsg.size() > 1) {
		if(splitmsg[1] == "search") {
			std::string input;
			for(std::string i : splitmsg) {
				if(!(i == splitmsg[0]) && !(i == splitmsg[1])) {
				    input = input + "%20" + i;
				}
			}
			std::string tempstr = msg.content;
			std::string msg = "Results for: `" + tempstr.erase(0,13) + "`\\n```";
			if(splitmsg.size() >= 3) {
				nlohmann::json j = nlohmann::json::parse(httpGet("https://initiate.host/search/" + input));
				int i = 0;
				while(i <= 4) {
					msg = msg + std::to_string(i+1) + ": " + rq(j["result"].at(i)["name"].dump()) + "\\n";
				i++;
				}
				msg = msg + "```";
				return msg;
			}
		}
		else if(splitmsg[1] == "info") {
			if(splitmsg.size() >= 3) {
				std::string input;
				for(std::string i : splitmsg) {
					if(!(i == splitmsg[0]) && !(i == splitmsg[1])) {
						input = input + "%20" + i;
					}
				}	
		    	nlohmann::json j = nlohmann::json::parse(httpGet("https://initiate.host/search/" + input));
				nlohmann::json j2 = nlohmann::json::parse(httpGet("https://initiate.host/anime/" + j["result"].at(0)["id"].dump()));
				std::string msg = "Title: `" + rq(j2["title"].dump())+ "`" +  
				"\\n" + "synopsis: ```" + rq(j2["synopsis"].dump()) + "```" + 
				"\\n" + "Aired from: `" + rq(j2["aired"]["from"].dump()) + "` to `" + rq(j2["aired"]["to"].dump()) + "`" +
				"\\n" + "Episodes: `" + j2["episode"].dump() + "`" +
				"\\n" + "Rating: `" + rq(j2["rating"].dump()) + "`" +
				"\\n" + "score: `" + j2["score"]["score"].dump() + "`" +
			    "\\n" + "url: <" + rq(j2["url"].dump())+ ">";
				return msg;
			}
		}
	}
	else {
	    return "Invalid use of `"+ conf.getPrefix() + "anime` \\nUse `" + conf.getPrefix() + "help anime`";
	}
}