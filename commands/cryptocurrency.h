#ifndef CRYPTOCURRENCY_H
#define CRYPTOCURRENCY_H
#include <vector>
#include <string>
#include <iostream>
#include "sleepy_discord/websocketpp_websocket.h"
#include "../config.h"

std::string cryptoCompare(SleepyDiscord::Message, config, std::vector<std::string>);

#endif