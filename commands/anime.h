#ifndef ANIME_H
#define ANIME_H
#include <string>
#include <vector>
#include <iostream>
#include "sleepy_discord/websocketpp_websocket.h"
#include "../config.h"

std::string myAnimeList(SleepyDiscord::Message, config, std::vector<std::string>);

#endif