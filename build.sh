tar -xzf sleepy-discord.tar.gz
mkdir build
cd build
cmake ..
make
cd ..
mkdir piebot
cp build/piebot piebot/piebot
cp config.txt piebot/config.txt