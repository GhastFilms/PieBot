#include "sleepy_discord/websocketpp_websocket.h"
#include "config.h"
#include <string>
#include <algorithm>
#include <random>
#include <vector>
#include <ctime>
#include <sstream>
#include <iostream>
#include <locale>
#include "json.hpp"
//classes
#include "classes/redditPost.h"
//commands
#include "commands/anime.h"
#include "commands/cryptocurrency.h"
#include "api_machine.h"


std::vector<std::string> split(std::string str,std::string sep){
    char* cstr=const_cast<char*>(str.c_str());
    char* current;
    std::vector<std::string> arr;
    current=strtok(cstr,sep.c_str());
    while(current!=NULL){
        arr.push_back(current);
        current=strtok(NULL,sep.c_str());
    }
    return arr;
}

config conf;

class myClientClass : public SleepyDiscord::DiscordClient {
private:
	
	enum commands {
		NA,
		KILL,
		SAY,
		HELP,
		INSULT,
		GROUP,
		MEME,
		CAT,
		CRYPTO,
		CRYPTOCURENCY,
		MAL,
		LETSGETLOUD
	};

	commands strtoenum(SleepyDiscord::Message msg) {
		if(msg.startsWith(conf.getPrefix() + "say" ) && conf.isModerator(msg.author.ID)) {return SAY;}
		else if(msg.startsWith(conf.getPrefix() + "help")) {return HELP;}
		else if(msg.startsWith(conf.getPrefix() + "insult") && msg.content.length() > 7 + conf.getPrefix().length()) {return INSULT;}
		//mod commands
		else if((msg.author.ID == conf.getOwnerID()) && msg.startsWith("$kill")) {return KILL;}
		//reddit commands
		else if(msg.startsWith(conf.getPrefix() + "meme")) {return MEME;}
		else if(msg.startsWith(conf.getPrefix() + "cat")) {return CAT;}
		//basic io commands
		else if(msg.startsWith(conf.getPrefix() + "letsgetloud")) {return LETSGETLOUD;}
		//experimental commands
		else if(conf.isModerator(msg.author.ID)) {
			if(msg.startsWith(conf.getPrefix() + "cc")) {return CRYPTOCURENCY;}
			else if(msg.startsWith(conf.getPrefix() + "anime")) {return MAL;}
			else if(msg.startsWith(conf.getPrefix() + "crypto")) {return CRYPTO;}
		}
		
		
		else {return NA;}
	};

public:
	using SleepyDiscord::DiscordClient::DiscordClient;
	void onMessage(SleepyDiscord::Message message) {
		std::vector<std::string> splitmsg = split(message.content, " ");
		switch(strtoenum(message)) {
			case NA: break;
			case KILL:
				sendMessage(message.channelID, "committing neck rope :)");
            	this -> quit();
				break;
			case SAY:
				std::cout << message.author.username << ": " << message.content << std::endl; 
				 if(message.content.length() >= 5) {
                    //make sure the bot isnt saying "@everyone" or "@here"
                    if((message.content.find("@everyone") != std::string::npos) || (message.content.find("@here") != std::string::npos)) {
                        sendMessage(message.channelID, "Input cannot contain `@everyone` or `@here`");
                    }
                    else {
                        sendMessage(message.channelID, message.content.erase(0,4));
                    }
                }
            	else {
                    sendMessage(message.channelID, "please provide some input");
                }
				break;
			case HELP:
				sendMessage(message.channelID, "```commands:``` \\n hello: says hello  \\n say: simple say command \\n insult: insult someone \\n cat: sends a cat \\n meme: sends a meme");
				break;
			case INSULT:
                if((message.content.find("@everyone") != std::string::npos) || (message.content.find("@here") != std::string::npos)) {
                    sendMessage(message.channelID, "Input cannot contain `@everyone` or `@here`");
                }
				else {
					sendMessage(
                    	message.channelID,
                    	split(message.content," ")[1] + " " +
                    	conf.getInsults()[(rand() + 1) % conf.getInsults().size()]
                	);
				}
				break;
			case MEME:
				sendMessage(message.channelID, conf.getMemes()[(rand() + 1) % (conf.getMemes().size() - 1)].getEmbed());
				//client.addReaction("channel id", "message id", "%F0%9F%98%95");
				//addReaction(message.channelID, message.ID, "%F0%9F%98%95");
				break;
			case CAT:
				sendMessage(message.channelID,conf.getCats()[(rand() + 1) % (conf.getCats().size() - 1)].getEmbed());
				break;
			case CRYPTO:
				if(splitmsg[1] == "sha256") {
				
					//sendMessage(message.channelID, SHA256HashString(message.content.erase(0,13)));
				}
				else if(splitmsg[1] == "md5") {
					
				}
				break;
			case LETSGETLOUD:
				sendMessage(message.channelID, "https://cdn.discordapp.com/attachments/488396157928603679/488396262643728385/AjOt0k-0ugPQrU63.mp4");
				break;
			case MAL:
				{
					sendMessage(message.channelID, myAnimeList(message, conf, splitmsg));
				break;
				}
			case CRYPTOCURENCY:
			{
				sendMessage(message.channelID, cryptoCompare(message, conf, splitmsg));
				break;
			}
		}
	}
};

int main() {
	srand(time(0));
	conf.init();
	std::cout << "Connecting to Discord" << std::endl;
	myClientClass client(conf.getToken(), 2);
	//client.updateStatus(conf.getPrefix() +  "help", 0);
	client.run();
}