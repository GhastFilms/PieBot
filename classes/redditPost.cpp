#include "redditPost.h"
#include "../json.hpp"
#include <string>
#include <iostream>
#include <algorithm>

using json = nlohmann::json;

redditPost::redditPost(std::string _author, std::string _url, std::string _subreddit, std::string _title, std::string _permalink, bool _isVideo, bool _nsfw, int _numComments, int _likes) {
    author = _author;
    url = _url;
    subreddit = _subreddit;
    title = _title;
    permalink = _permalink;
    isVideo = _isVideo;
    nsfw = _nsfw;
    numComments = _numComments;
    likes = _likes;
}

void redditPost::set(std::string _author, std::string _url, std::string _subreddit, std::string _title, std::string _permalink, bool _isVideo, bool _nsfw, int _numComments, int _likes) {
    author = _author;
    url = _url;
    subreddit = _subreddit;
    title = _title;
    permalink = _permalink;
    isVideo = _isVideo;
    nsfw = _nsfw;
    numComments = _numComments;
    likes = _likes;
}

std::string redditPost::getEmbed() {
/*

sleepy discord cant do em

json msgt =
    {"embed", {
            {"title",title},
            {"author",{ 
                    {"name",("u/" + author)},
                    {"url",("https://www.reddit.com/u/" + author)} }
            },
            {"image",{{"url", url}}}
        }
    };
    //std::string msg = msgt.dump();
    //std::replace(msg.begin(), msg.end(), '\\', "\\\\");
    std::string msg = 
    std::cout << msg << std::endl;
*/
    std::string msg = "`" + title + "` - `u/" + author + "` from `r/" + subreddit +
    + "`\\n" + url;
    return msg;
};


std::string redditPost::getURL() {
    return url;
};
        
std::string redditPost::getAuthor() {
    return author;
};
        
std::string redditPost::getSubreddit() {
    return subreddit;
};
        
std::string redditPost::getTitle(){
    return title;
};

/*

{
  "embed": {
  "title": "Restoration 100",
  "author": {
    "name": "u/TheDarthKnightBegins",
    "url": "https://www.reddit.com/u/TheDarthKnightBegins"
  },
  "image": {"url": "https://i.redditmedia.com/sltuA4tSaTY3YxVR7YWQ3ELlbU7DxGdlc7scmytpOe0.jpg?fit=crop&crop=faces%2Centropy&arh=2&w=640&s=29cf7d5a3f087e2a7cc55e004e113dc0"}
  }
}


*/