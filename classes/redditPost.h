#ifndef REDDITPOST_H
#define REDDITPOST_H

#include <string>
#include "../json.hpp"

using json = nlohmann::json;

class redditPost {
    public:
        redditPost(std::string = "", std::string = "", std::string = "", std::string = "", std::string = "", bool = false, bool = false, int = 0, int = 0);
        void set(std::string = "", std::string = "", std::string = "", std::string = "", std::string = "", bool = false, bool = false, int = 0, int = 0);
        std::string getEmbed();
        std::string getURL();
        std::string getAuthor();
        std::string getSubreddit();
        std::string getTitle();
    private:
        std::string url;
        std::string subreddit;
        std::string author;
        std::string title;
        std::string permalink;
        bool nsfw;
        bool isVideo;
        int numComments;
        int likes;
};
#endif