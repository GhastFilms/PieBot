#include <iostream>
#include <string>
#include <stdio.h>
#include <curl/curl.h>
#include <vector>
#include "json.hpp"
#include "classes/redditPost.h"

using json = nlohmann::json;

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

std::string httpGet(std::string URL)
{
  CURL *curl;
  CURLcode res;
  std::string readBuffer;
  curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, URL.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
    res = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
  return readBuffer;
}

std::string rq(std::string input) {
  input.erase(0,1);
  input.erase(input.length()-1,input.length());
  return input;
}

bool strToBool(std::string input) {
  if(input == "true") {return true;}
  else if (input == "false") {return false;}
}

std::vector<redditPost> getRPostObjs(std::string input) {
  json j = json::parse(httpGet(input));
  std::vector<redditPost> memes;
  int i = 0;
  redditPost temp;
  std::string author, URL, subreddit, title, permalink;
  bool nsfw, isVideo;
  int likes, numComments;
  while(i <= j["data"]["children"].size() - 1) {
    if(!strToBool(j["data"]["children"].at(i)["data"]["pinned"].dump())) {
      author = rq(j["data"]["children"].at(i)["data"]["author"].dump());
      URL = rq(j["data"]["children"].at(i)["data"]["url"].dump());
      subreddit = rq(j["data"]["children"].at(i)["data"]["subreddit"].dump());
      title = rq(j["data"]["children"].at(i)["data"]["title"].dump());
      permalink = rq(j["data"]["children"].at(i)["data"]["permalink"].dump());
      nsfw = strToBool(j["data"]["children"].at(i)["data"]["over_18"].dump());
      isVideo = strToBool(j["data"]["children"].at(i)["data"]["is_video"].dump());
      numComments = std::stoi(j["data"]["children"].at(i)["data"]["num_comments"].dump());
      likes = std::stoi(j["data"]["children"].at(i)["data"]["ups"].dump());  
      temp.set(author, URL, subreddit, title, permalink, isVideo, nsfw, numComments, likes);
      memes.push_back(temp);
    }
    i++;
  }
  return memes;
}

/*
int main() {
  std::vector<redditPost> test = getRPostObjs("https://www.reddit.com/r/dankmemes/hot.json?limit=100");
  int i = 0;
  while(i <= test.size()-1) {
    std::cout << test[i].getTitle() << std::endl;
    i++;
  }
}
*/