#ifndef CONFIG_H
#define CONFIG_H

#include <string>
#include <vector>
#include "classes/redditPost.h"
#include "json.hpp"


class config {
	public:
		std::string getToken();
		std::string getPrefix();
		std::string getModPrefix();
		bool isModerator(std::string);
		std::vector<std::string> getModerators();
		std::vector<std::string> getInsults();
		std::vector<redditPost> getMemes();
		std::vector<redditPost> getCats();
		std::string getOwnerID();
		void init();
	private:
		std::string modPrefix;
		std::string token;
		std::string prefix;
		std::string ownerID;
		std::vector<std::string> moderators;
		std::vector<std::string> insults;
		std::vector<redditPost> cats;
		std::vector<redditPost> memes;
};
#endif
