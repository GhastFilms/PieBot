# PieBot

A Discord bot written in c++ for the DrinkablePie Discord servers.

The bot is build using the [sleepy discord](https://github.com/yourWaifu/sleepy-discord) library made by [yourWaifu](https://github.com/yourWaifu).

Sleepy Discord is included in the repo as a tar.gz.

Piebot is being developed on Ubuntu 18.04.
